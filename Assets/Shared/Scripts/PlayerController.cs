﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObject {
    
    public float _maxSpeed = 7;
    public float _jumpForce = 7;
    
    private SpriteRenderer _renderer;
    private Animator _animator;
    void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }
    
    protected override void ComputeVeclocity()
    {
        Vector2 tMove = Vector2.zero;
        tMove.x = Input.GetAxis("Horizontal");
        if(Input.GetButtonDown("Jump") && _grounded)
        {
            _velocity.y = _jumpForce;
        } 
        else if (Input.GetButtonUp("Jump"))
        {
            if(_velocity.y > 0)
            {
                _velocity.y *= 0.5f;
            }
        }

        bool tFlipSprite = (_renderer.flipX ? (tMove.x > 0.01f) : (tMove.x < -0.01f));

        if(tFlipSprite)
        {
            _renderer.flipX = !_renderer.flipX;
        }

        _animator.SetFloat("HorizontalVelocity", Mathf.Abs(tMove.x));
        _animator.SetFloat("VerticalVelocity", _velocity.y);
        _animator.SetBool("Grounded", _grounded);
        _targetVelocity = tMove * _maxSpeed;
    }
}
