﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupController : MonoBehaviour {
	
	public float spinSpeed = 30f;
	public GameObject rupeeMesh;
	public GameObject particleSys;

	private float particleSysDuration;
	private Collider2D collider;

	void Start()
	{
		particleSysDuration = particleSys.GetComponent<ParticleSystem>().main.duration;
		collider = GetComponent<Collider2D>();
	}
	void Update()
	{
		rupeeMesh.transform.Rotate(0, 0, spinSpeed * Time.deltaTime);
	}
	void OnTriggerEnter2D()
	{
		collider.enabled = false;
		rupeeMesh.SetActive(false);
		particleSys.SetActive(true);
		Invoke("DisableRupee", particleSysDuration);
	}

	void DisableRupee()
	{
		gameObject.SetActive(false);
	}
}
